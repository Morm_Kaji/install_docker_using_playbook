Install docker using Ansible
# Create host in hosts file
$ vim /etc/ansible/host

# Configuration file 
[docker]
ip-address-of-slave

# Create playbook directory
$ mkdir playbook

# Create playbook
$ vim docker_install.yml

# Configure for install docker
---
- hosts: ssl
  become: yes
  vars_prompt:
     - name: user
       prompt: Existing User
       private: no
  tasks:
     - name: install dependency
       apt:
               name: "{{item}}"
               state: present
               update_cache: yes
       loop:
               - apt-transport-https
               - ca-certificates
               - curl
               - gnupg-agent
               - software-properties-common
     - name: Add GPG
       apt_key:
               url: https://download.docker.com/linux/ubuntu/gpg
               state: present
     - name: setup repository docker
       apt_repository:
               repo: deb https://download.docker.com/linux/ubuntu bionic stable
               state: present
     - name: installa docker
       apt:
               name: "{{item}}"
               state: latest
               update_cache: yes
       loop:
               - docker-ce
               - docker-ce-cli
               - containerd.io
     - name: setup service docker
       service:
               name: docker
               state: started
               enabled: yes
     - name: adding existing user to group docker
       user:
               name: "{{user}}"
               groups: docker
               append: yes
              
  handlers:
      - name: restart docker
        service:
               name: docker 
               state: restarted

# Start install with passing sudo password
$ ansible-playbook test.yml --extra-vars "ansible_sudo_pass=password"
